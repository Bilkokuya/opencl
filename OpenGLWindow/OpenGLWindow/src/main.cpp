#include <SFML\Window.hpp>
#include <Windows.h>
#include <gl\GL.h>
#include <CL\opencl.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

namespace gdm
{
	namespace cl
	{
		struct face
		{
			cl_float4 vertices[3];
		};
	}
}

GLuint texture_id;
const int texture_width = 640;
const int texture_height = 360;
const std::string window_name = "OpenGL Window";
const std::string opencl_file = "kernels/texture.c";
const std::string kernel_name = "TEXTURING";

cl_context context;
cl_program program;
cl_kernel kernel;
cl_device_id device_id;
cl_platform_id platform_id;

float camerax = 0.0f;
float cameray = 0.0f;
float cameraz = 0.0f;

cl_mem buffer_texture;
cl_mem buffer_dimensions;
cl_mem buffer_geometry;
cl_mem buffer_geometry_size;
cl_mem buffer_camera;

std::vector< cl_float4 >		dataset_a;
std::vector< float >			dataset_b;
std::vector< gdm::cl::face >	dataset_c;
std::vector< int >				dataset_d;
std::vector< cl_float4 >		dataset_e;

cl_command_queue queue;

sf::Time timer;
float last_time = 0.0f;

void InitialiseOpenCL();
void OpenCLWork();
void GameLogicUpdate();
void Update( void );
void Render( void );
cl_float4 vertice( float x, float y, float z, float w );
void check_error (cl_int error);

int CALLBACK WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
	/* setup window */
	sf::Window window( sf::VideoMode( 640, 360 ), window_name.c_str() );

	InitialiseOpenCL();

	/* setup openGL texture */
	glGenTextures( 1, &texture_id );

	/* main loop */
	while( window.isOpen() )
	{
		sf::Event event;
		while( window.pollEvent( event ) )
		{
			if ( event.type == sf::Event::Closed )
			{
				window.close();
			}
			else if ( event.type == sf::Event::Resized )
			{
				glViewport(0, 0, window.getSize().x, window.getSize().y );
			}
		}

		Update();
		Render();

		window.display();
	}
}

void InitialiseOpenCL()
{
	/* setup openCL context */
	cl_uint platform_id_count = 0;
	clGetPlatformIDs( 0, NULL, &platform_id_count );
	std::vector< cl_platform_id > platform_ids( platform_id_count );
	clGetPlatformIDs( platform_id_count, platform_ids.data(), NULL );
	platform_id = platform_ids[0];

	cl_uint device_id_count = 0;
	clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_ALL, 0, NULL, &device_id_count );
	std::vector< cl_device_id > device_ids( device_id_count );
	clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_ALL, device_id_count, device_ids.data(), NULL );
	device_id = device_ids[0];

	/* create the context */
	const cl_context_properties context_properties[] = {
		CL_CONTEXT_PLATFORM, ( cl_context_properties )( platform_id ),
		CL_GL_CONTEXT_KHR, ( cl_context_properties )( wglGetCurrentContext() ),
		0,0
	};

	cl_int error = CL_SUCCESS;
	context = clCreateContext(
		context_properties,
		device_id_count,
		device_ids.data(),
		NULL,
		NULL,
		&error
	);
	check_error( error );

	/* load the kernel */
	std::ifstream kernel_file( opencl_file );
	std::string kernel_source( 
		( std::istreambuf_iterator< char > ( kernel_file ) ),
		std::istreambuf_iterator< char >() );

	/* create the program */
	const int count = 1;
	size_t lengths[ count ] = { kernel_source.size() };
	const char* sources[ count ] = { kernel_source.data() };
	program = clCreateProgramWithSource(
		context,
		count,
		sources,
		lengths,
		&error
		);
	check_error( error );

	error = clBuildProgram(
		program,
		device_id_count,
		device_ids.data(),
		NULL,
		NULL,
		NULL
		);
	if ( error != CL_SUCCESS )
	{
		 // Determine the size of the log
		size_t log_size;
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// Allocate memory for the log
		char *log = (char *) malloc(log_size);

		// Get the log
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		std::ofstream build_log("build_log.txt");
		build_log << log;
		build_log.close();

		free( log );
	}
	check_error( error );

	/* create the kernel*/
	kernel = clCreateKernel(
		program,
		kernel_name.c_str(),
		&error
		);
	check_error( error );

	dataset_a.resize( texture_width * texture_height );
	dataset_b.resize( 2 );
	dataset_d.resize( 1 );
	dataset_e.resize( 4 );

	buffer_texture = clCreateBuffer(
		context,
		CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
		sizeof( cl_float4 ) * dataset_a.size(),
		dataset_a.data(),
		&error
		);
	check_error( error );

	dataset_b[0] = texture_width;
	dataset_b[1] = texture_height;
	buffer_dimensions = clCreateBuffer(
		context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof( float ) * dataset_b.size(),
		dataset_b.data(),
		&error
	);
	check_error( error );

	
	gdm::cl::face faceA;
	faceA.vertices[0] = vertice( 0.0f, 0.5f, 0.1f, 1.0f );
	faceA.vertices[1] = vertice( 0.5f, 0.8f, 0.3f, 1.0f );
	faceA.vertices[2] = vertice( 0.8f, 0.5f, 0.4f, 1.0f );

	gdm::cl::face faceB;
	faceB.vertices[0] = vertice( 0.0f, 0.5f, 0.1f, 1.0f );
	faceB.vertices[1] = vertice( -0.5f, -0.8f, 0.2f, 1.0f );
	faceB.vertices[2] = vertice( -0.8f, 0.5f, 0.3f, 1.0f );

	gdm::cl::face faceC;
	faceC.vertices[0] = vertice( 0.2f, 0.4f, 0.1f, 1.0f );
	faceC.vertices[1] = vertice( 0.3f, 0.6f, 0.1f, 1.0f );
	faceC.vertices[2] = vertice( 0.9f, 0.7f, 0.5f, 1.0f );

	dataset_c.push_back( faceA );
	dataset_c.push_back( faceB );
	dataset_c.push_back( faceC );

	buffer_geometry = clCreateBuffer(
		context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof( gdm::cl::face) * dataset_c.size(),
		dataset_c.data(),
		&error
	);
	check_error( error );
	
	dataset_d[0] = dataset_c.size();
	buffer_geometry_size = clCreateBuffer(
		context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof( int ) * dataset_d.size(),
		dataset_d.data(),
		&error
	);
	check_error( error );

	dataset_e[0].s[0] = 1.0f;
	dataset_e[1].s[0] = 0.0f;
	dataset_e[2].s[0] = 0.0f;
	dataset_e[3].s[0] = 0.0f;

	dataset_e[0].s[1] = 0.0f;
	dataset_e[1].s[1] = 1.0f;
	dataset_e[2].s[1] = 0.0f;
	dataset_e[3].s[1] = 0.0f;

	dataset_e[0].s[2] = 0.0f;
	dataset_e[1].s[2] = 0.0f;
	dataset_e[2].s[2] = 1.0f;
	dataset_e[3].s[2] = 0.0f;

	dataset_e[0].s[3] = camerax;
	dataset_e[1].s[3] = cameray;
	dataset_e[2].s[3] = cameraz;
	dataset_e[3].s[3] = 1.0f;

	buffer_camera = clCreateBuffer(
		context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof( cl_float4 ) * dataset_e.size(),
		dataset_e.data(),
		&error
	);
	check_error( error );

	queue = clCreateCommandQueue( context, device_id, 0, &error );
	check_error( error );

	clSetKernelArg( kernel, 0, sizeof( cl_mem ), &buffer_texture );
	clSetKernelArg( kernel, 1, sizeof( cl_mem ), &buffer_dimensions );
	clSetKernelArg( kernel, 2, sizeof( cl_mem ), &buffer_geometry );
	clSetKernelArg( kernel, 3, sizeof( cl_mem ), &buffer_geometry_size );
	clSetKernelArg( kernel, 4, sizeof( cl_mem ), &buffer_camera );

	const size_t global_work_size[] = { texture_width, texture_height };
	error = clEnqueueNDRangeKernel(
		queue,
		kernel,
		2,
		NULL,
		global_work_size,
		NULL,
		0,
		NULL,
		NULL
		);
	check_error( error );

	/* get the results */
	error = clEnqueueReadBuffer(
		queue,
		buffer_texture,
		CL_TRUE,
		0,
		sizeof( cl_float4 ) * dataset_a.size(),
		dataset_a.data(),
		0,
		NULL,
		NULL
		);
	check_error( error );

	glBindTexture( GL_TEXTURE_2D, texture_id );

	/* default colour values */
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, texture_width, texture_height, 0, GL_RGBA, GL_FLOAT, dataset_a.data() );

	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
}

void GameLogicUpdate()
{
	float dt = 1.0f;//timer.asSeconds() - last_time;
	last_time = timer.asSeconds();

	float speed = 0.02f;

	if ( sf::Keyboard::isKeyPressed( sf::Keyboard::A ) )
	{
		camerax -= speed * dt;
	}
	else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::D ) )
	{
		camerax += speed * dt;
	}

	if ( sf::Keyboard::isKeyPressed( sf::Keyboard::W ) )
	{
		cameray += speed * dt;
	}
	else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::S ) )
	{
		cameray -= speed * dt;
	}

	if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Q ) )
	{
		cameraz += speed * dt;
	}
	else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::E ) )
	{
		cameraz -= speed * dt;
	}
	 
}

void OpenCLWork()
{
	int error;
	std::vector< cl_float4 > dataset_e( 4 );

	dataset_e[0].s[0] = 1.0f;
	dataset_e[1].s[0] = 0.0f;
	dataset_e[2].s[0] = 0.0f;
	dataset_e[3].s[0] = 0.0f;

	dataset_e[0].s[1] = 0.0f;
	dataset_e[1].s[1] = 1.0f;
	dataset_e[2].s[1] = 0.0f;
	dataset_e[3].s[1] = 0.0f;

	dataset_e[0].s[2] = 0.0f;
	dataset_e[1].s[2] = 0.0f;
	dataset_e[2].s[2] = 1.0f;
	dataset_e[3].s[2] = 0.0f;

	dataset_e[0].s[3] = camerax;
	dataset_e[1].s[3] = cameray;
	dataset_e[2].s[3] = cameraz;
	dataset_e[3].s[3] = 1.0f;

	clEnqueueWriteBuffer(
		queue,
		buffer_camera,
		CL_FALSE,
		0,
		sizeof( cl_float4 ) * dataset_e.size(),
		dataset_e.data(),
		0,
		0,
		0
	);

	clSetKernelArg( kernel, 0, sizeof( cl_mem ), &buffer_texture );
	clSetKernelArg( kernel, 1, sizeof( cl_mem ), &buffer_dimensions );
	clSetKernelArg( kernel, 2, sizeof( cl_mem ), &buffer_geometry );
	clSetKernelArg( kernel, 3, sizeof( cl_mem ), &buffer_geometry_size );
	clSetKernelArg( kernel, 4, sizeof( cl_mem ), &buffer_camera );

	const size_t global_work_size[] = { texture_width, texture_height };
	error = clEnqueueNDRangeKernel(
		queue,
		kernel,
		2,
		NULL,
		global_work_size,
		NULL,
		0,
		NULL,
		NULL
		);
	check_error( error );

	/* get the results */
	error = clEnqueueReadBuffer(
		queue,
		buffer_texture,
		CL_TRUE,
		0,
		sizeof( cl_float4 ) * dataset_a.size(),
		dataset_a.data(),
		0,
		NULL,
		NULL
		);
	check_error( error );

	glBindTexture( GL_TEXTURE_2D, texture_id );

	/* default colour values */
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, texture_width, texture_height, 0, GL_RGBA, GL_FLOAT, dataset_a.data() );

	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
}

/* work with openCL to edit the texture each frame */
void Update( void )
{
	GameLogicUpdate();
	OpenCLWork();
}

/* render the texture to the screen */
void Render( void )
{
	glClearColor( 1.0f, 0.0f, 0.0f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );

	glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, texture_id );

	const float size = 0.9f;
	glBegin( GL_QUADS );
		glTexCoord2d( 0.0, 0.0 ); glVertex2d( -size,-size );
		glTexCoord2d( 1.0, 0.0 ); glVertex2d(  size,-size );
		glTexCoord2d( 1.0, 1.0 ); glVertex2d(  size, size );
		glTexCoord2d( 0.0, 1.0 ); glVertex2d( -size, size );
	glEnd();
}

void check_error (cl_int error)
{
	if (error != CL_SUCCESS) {
		std::cerr << "OpenCL call failed with error " << error << std::endl;

		char buffer[1024];
		sprintf( buffer, "OpenCL call failed with error: %d", error );

		MessageBoxA( NULL, buffer, "failed", MB_OK );
		std::exit (1);
	}
}

cl_float4 vertice( float x, float y, float z, float w )
{
	cl_float4 vertice;
	vertice.s[0] = x;
	vertice.s[1] = y;
	vertice.s[2] = z;
	vertice.s[3] = w;
	return vertice;
}
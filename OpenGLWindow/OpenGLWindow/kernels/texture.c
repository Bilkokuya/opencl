
#define EPSILON 0.000001f;

typedef struct face_t
{
	float4 vertices[3];
} face;

typedef struct ray_t
{
	float4 origin;
	float4 direction;
} ray;

__kernel void TEXTURING( __global float4* image, __global float2* image_dimensions, __global face* geometry, __global int* geometry_size, __global float4* camera_matrix )
{
	const int x = get_global_id( 0 );
	const int y = get_global_id( 1 );
	const int index = y * (*image_dimensions).x + x;

	/* create the ray */
	float4 a = (float4)( 0.0f, 0.0f, -1.0f, 1.0f);
	float4 b = (float4)( ((2*x) / (*image_dimensions).x) - 1.0f, ((2*y) / (*image_dimensions).y) - 1.0f, 0.0f, 1.0f );

	float4 origin = (float4)(
		dot( a, camera_matrix[0] ),
		dot( a, camera_matrix[1] ),
		dot( a, camera_matrix[2] ),
		dot( a, camera_matrix[3] )
		);

	float4 end = (float4)(
		dot( b, camera_matrix[0] ),
		dot( b, camera_matrix[1] ),
		dot( b, camera_matrix[2] ),
		dot( b, camera_matrix[3] )
		);

	ray r = { origin, end - origin };

	/* do the ray intersection*/
	float4 colour = (float4)(0.0f,0.0f,0.0f,0.0f);
	float closestT = MAXFLOAT;

	/* ray test against every triangle */
	for( int i = 0; i < (*geometry_size); ++i )
	{
		face f		= geometry[i];
		float4 a	= f.vertices[0];
		float4 b	= f.vertices[1];
		float4 c	= f.vertices[2];
		float4 d	= r.direction;
		float4 C	= r.origin - c;

		float detM = dot( -(cross((a-c),-d)), (b-c) );
		float detU = dot( -(cross( C,   -d)), (b-c) );
		float detV = dot( -(cross((a-c),-d)), (C) );
		float detT = dot( -(cross((a-c), C)), (b-c) );

		float u = detU / detM;
		float v = detV / detM;
		float w = 1 - u - v;
		float t = detT / detM;

		if ( t < closestT && t > 0.0000001f && u + v < 1.0f && u > 0.0000001f && v > 0.0000001f )
		{
			closestT = t;
			colour = (float4)(t/2.0f,t/2.0f,t/2.0f,1.0f);
		}
	}

	/* render the pixel*/
	image[ index ] = colour;
}

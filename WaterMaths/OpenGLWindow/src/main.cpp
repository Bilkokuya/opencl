#include <SFML\Window.hpp>
#include <Windows.h>
#include <gl\GL.h>
#include <CL\opencl.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

namespace gdm
{
	namespace cl
	{
		struct face
		{
			cl_float4 vertices[3];
		};
	}
}

GLuint texture_id;
const int texture_width = 128;
const int texture_height = 128;
const std::string window_name = "OpenGL Window";
const std::string opencl_file = "kernels/texture.c";
const std::string kernel_name = "TEXTURING";

cl_context context;
cl_program program;
cl_kernel kernel;
cl_device_id device_id;
cl_platform_id platform_id;

cl_mem buffer_n;
cl_mem buffer_n1;
cl_mem buffer_dimensions;
cl_mem buffer_constants;

std::vector< cl_float >		dataset_n;
std::vector< cl_float >		dataset_n1;
std::vector< cl_int2 >		dataset_dimensions;
std::vector< cl_float4 >	dataset_constants;

cl_command_queue queue;

sf::Time timer;
float last_time = 0.0f;

void InitialiseOpenCL();
void OpenCLWork();
void GameLogicUpdate();
void Update( void );
void Render( void );
cl_float4 vertice( float x, float y, float z, float w );
void check_error (cl_int error);

int CALLBACK WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
	/* setup window */
	sf::Window window( sf::VideoMode( 128, 128 ), window_name.c_str() );

	InitialiseOpenCL();

	/* setup openGL texture */
	glGenTextures( 1, &texture_id );

	/* main loop */
	while( window.isOpen() )
	{
		sf::Event event;
		while( window.pollEvent( event ) )
		{
			if ( event.type == sf::Event::Closed )
			{
				window.close();
			}
			else if ( event.type == sf::Event::Resized )
			{
				glViewport(0, 0, window.getSize().x, window.getSize().y );
			}
		}

		Update();
		Render();

		window.display();
	}
}

void InitialiseOpenCL()
{
	/* setup openCL context */
	cl_uint platform_id_count = 0;
	clGetPlatformIDs( 0, NULL, &platform_id_count );
	std::vector< cl_platform_id > platform_ids( platform_id_count );
	clGetPlatformIDs( platform_id_count, platform_ids.data(), NULL );
	platform_id = platform_ids[0];

	cl_uint device_id_count = 0;
	clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_ALL, 0, NULL, &device_id_count );
	std::vector< cl_device_id > device_ids( device_id_count );
	clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_ALL, device_id_count, device_ids.data(), NULL );
	device_id = device_ids[0];

	/* create the context */
	const cl_context_properties context_properties[] = {
		CL_CONTEXT_PLATFORM, ( cl_context_properties )( platform_id ),
		CL_GL_CONTEXT_KHR, ( cl_context_properties )( wglGetCurrentContext() ),
		0,0
	};

	cl_int error = CL_SUCCESS;
	context = clCreateContext(
		context_properties,
		device_id_count,
		device_ids.data(),
		NULL,
		NULL,
		&error
	);
	check_error( error );

	/* load the kernel */
	std::ifstream kernel_file( opencl_file );
	std::string kernel_source( 
		( std::istreambuf_iterator< char > ( kernel_file ) ),
		std::istreambuf_iterator< char >() );

	/* create the program */
	const int count = 1;
	size_t lengths[ count ] = { kernel_source.size() };
	const char* sources[ count ] = { kernel_source.data() };
	program = clCreateProgramWithSource(
		context,
		count,
		sources,
		lengths,
		&error
		);
	check_error( error );

	error = clBuildProgram(
		program,
		device_id_count,
		device_ids.data(),
		NULL,
		NULL,
		NULL
		);
	if ( error != CL_SUCCESS )
	{
		 // Determine the size of the log
		size_t log_size;
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// Allocate memory for the log
		char *log = (char *) malloc(log_size);

		// Get the log
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		std::ofstream build_log("build_log.txt");
		build_log << log;
		build_log.close();

		free( log );
	}
	check_error( error );

	/* create the kernel*/
	kernel = clCreateKernel(
		program,
		kernel_name.c_str(),
		&error
		);
	check_error( error );

	dataset_n.resize( texture_width * texture_height );
	dataset_n1.resize(  texture_width * texture_height );
	dataset_dimensions.resize( 1 );
	dataset_constants.resize( 1 );

	buffer_n = clCreateBuffer(
		context,
		CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
		sizeof( cl_float ) * dataset_n.size(),
		dataset_n.data(),
		&error
		);
	check_error( error );

	buffer_n1 = clCreateBuffer(
		context,
		CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
		sizeof( cl_float ) * dataset_n1.size(),
		dataset_n1.data(),
		&error
		);
	check_error( error );
	
	cl_int2 dimensions;
	dimensions.s[0] = texture_width;
	dimensions.s[1] = texture_height;
	dataset_dimensions[0] =  dimensions ;

	buffer_dimensions = clCreateBuffer(
		context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof( cl_int2 ) * dataset_dimensions.size(),
		dataset_dimensions.data(),
		&error
	);
	check_error( error );
	
	cl_float4 constants;
	constants.s[0] = 1; // h
	constants.s[1] = 0.5; // u
	constants.s[2] = 1; // c
	constants.s[3] = 0.1; // dt
	dataset_constants[0] = constants;

	buffer_constants = clCreateBuffer(
		context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof( cl_float4 ) * dataset_constants.size(),
		dataset_constants.data(),
		&error
	);
	check_error( error );

	queue = clCreateCommandQueue( context, device_id, 0, &error );
	check_error( error );

	clSetKernelArg( kernel, 0, sizeof( cl_mem ), &buffer_n );
	clSetKernelArg( kernel, 1, sizeof( cl_mem ), &buffer_n1 );
	clSetKernelArg( kernel, 2, sizeof( cl_mem ), &buffer_dimensions );
	clSetKernelArg( kernel, 3, sizeof( cl_mem ), &buffer_constants);

	const size_t global_work_size[] = { texture_width, texture_height };
	error = clEnqueueNDRangeKernel(
		queue,
		kernel,
		2,
		NULL,
		global_work_size,
		NULL,
		0,
		NULL,
		NULL
		);
	check_error( error );

	/* get the results */
	error = clEnqueueReadBuffer(
		queue,
		buffer_n,
		CL_TRUE,
		0,
		sizeof( cl_float ) * dataset_n.size(),
		dataset_n.data(),
		0,
		NULL,
		NULL
		);
	check_error( error );

	glBindTexture( GL_TEXTURE_2D, texture_id );

	/* default colour values */
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, texture_width, texture_height, 0, GL_RED, GL_FLOAT, dataset_n.data() );

	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
}

void GameLogicUpdate()
{
	float dt = 1.0f;
	last_time = timer.asSeconds();
}

void OpenCLWork()
{
	int error;

	if ( sf::Keyboard::isKeyPressed( sf::Keyboard::W ) )
	{
		dataset_n[ ((texture_height/2) * texture_width) + texture_width/2 ] = 5.0f;
	}

	clEnqueueWriteBuffer(
		queue,
		buffer_n,
		CL_FALSE,
		0,
		sizeof( cl_float ) * dataset_n.size(),
		dataset_n.data(),
		0,
		0,
		0
	);

	clSetKernelArg( kernel, 0, sizeof( cl_mem ), &buffer_n );
	clSetKernelArg( kernel, 1, sizeof( cl_mem ), &buffer_n1 );
	clSetKernelArg( kernel, 2, sizeof( cl_mem ), &buffer_dimensions );
	clSetKernelArg( kernel, 3, sizeof( cl_mem ), &buffer_constants);

	const size_t global_work_size[] = { texture_width, texture_height };
	error = clEnqueueNDRangeKernel(
		queue,
		kernel,
		2,
		NULL,
		global_work_size,
		NULL,
		0,
		NULL,
		NULL
		);
	check_error( error );

	/* get the results */
	error = clEnqueueReadBuffer(
		queue,
		buffer_n,
		CL_TRUE,
		0,
		sizeof( cl_float ) * dataset_n.size(),
		dataset_n.data(),
		0,
		NULL,
		NULL
		);
	check_error( error );

	glBindTexture( GL_TEXTURE_2D, texture_id );

	/* default colour values */
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, texture_width, texture_height, 0, GL_RED, GL_FLOAT, dataset_n.data() );

	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
}

/* work with openCL to edit the texture each frame */
void Update( void )
{
	GameLogicUpdate();
	OpenCLWork();
}

/* render the texture to the screen */
void Render( void )
{
	glClearColor( 1.0f, 0.0f, 0.0f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );

	glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, texture_id );

	const float size = 0.9f;
	glBegin( GL_QUADS );
		glTexCoord2d( 0.0, 0.0 ); glVertex2d( -size,-size );
		glTexCoord2d( 1.0, 0.0 ); glVertex2d(  size,-size );
		glTexCoord2d( 1.0, 1.0 ); glVertex2d(  size, size );
		glTexCoord2d( 0.0, 1.0 ); glVertex2d( -size, size );
	glEnd();
}

void check_error (cl_int error)
{
	if (error != CL_SUCCESS) {
		std::cerr << "OpenCL call failed with error " << error << std::endl;

		char buffer[1024];
		sprintf( buffer, "OpenCL call failed with error: %d", error );

		MessageBoxA( NULL, buffer, "failed", MB_OK );
		std::exit (1);
	}
}

cl_float4 vertice( float x, float y, float z, float w )
{
	cl_float4 vertice;
	vertice.s[0] = x;
	vertice.s[1] = y;
	vertice.s[2] = z;
	vertice.s[3] = w;
	return vertice;
}
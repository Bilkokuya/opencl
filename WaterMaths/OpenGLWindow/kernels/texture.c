
#define EPSILON 0.000001f;

int index( int x, int y, int width, int height )
{
	int i = min( max( x, 0 ), width );
	int j = min( max( y, 0 ), height );

	return (j * width) + i;
}

__kernel void TEXTURING( __global float* n, __global float* nm1, __global int2* dimensions, __global float4* constants )
{
	const int i = get_global_id( 0 );
	const int j = get_global_id( 1 );

	const float h = (*constants).x;
	const float u = (*constants).y;
	const float c = (*constants).z;
	const float dt = (*constants).w;

	/* calculation */
	float z_nm1_ij	= nm1[ index( i, j, (*dimensions).x, (*dimensions).y ) ];
	float z_n_ij	= n[ index( i, j, (*dimensions).x, (*dimensions).y ) ];
	float z_n_i1j	= n[ index( i + 1, j, (*dimensions).x, (*dimensions).y ) ];
	float z_n_im1j	= n[ index( i - 1, j, (*dimensions).x, (*dimensions).y  ) ];
	float z_n_ijm1	= n[ index( i, j - 1, (*dimensions).x, (*dimensions).y  ) ];
	float z_n_ij1	= n[ index( i, j + 1, (*dimensions).x, (*dimensions).y  ) ];

	float A = (4-(8*(c*c)*(dt*dt)/(h*h))) / (u*dt + 2 );
	float B = (u*dt - 2) / (u*dt + 2);
	float C = (2*(c*c)*(dt*dt)/(h*h) ) / (u*dt + 2);
	float z_n1_ij = (A * z_n_ij) + (B * z_nm1_ij) + (C * (z_n_i1j + z_n_im1j + z_n_ijm1 + z_n_ij1));
	

	/* render the pixel*/
	nm1[ index(i,j, (*dimensions).x, (*dimensions).y) ] = z_n_ij;
	n[ index(i,j, (*dimensions).x, (*dimensions).y) ] = z_n1_ij;
}
